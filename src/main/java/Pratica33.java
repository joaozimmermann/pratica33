import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz orig = new Matriz(3, 2);
        Matriz matriz2 = new Matriz(3,2);
        Matriz matriz3 = new Matriz(2,3);
        double[][] m = orig.getMatriz();
        m[0][0] = 0.0;
        m[0][1] = 0.1;
        m[1][0] = 1.0;
        m[1][1] = 1.1;
        m[2][0] = 2.0;
        m[2][1] = 2.1;
        Matriz transp = orig.getTransposta();
        System.out.println("Matriz original: " + orig);
        System.out.println("Matriz transposta: " + transp);
        
        double[][] n = matriz2.getMatriz();
        n[0][0] = 0.0;
        n[0][1] = 0.1;
        n[1][0] = 1.0;
        n[1][1] = 1.1;
        n[2][0] = 2.0;
        n[2][1] = 2.1;
        Matriz somada = orig.soma(matriz2);
        System.out.println("Matriz original: " + matriz2);
        System.out.println("Matriz somada: " + somada);
        
        double[][] o = matriz3.getMatriz();
        o[0][0] = 1.0;
        o[0][1] = 0.1;
        o[0][2] = 0.2;
        o[1][0] = 1.0;
        o[1][1] = 1.1;
        o[1][2] = 1.2;
        Matriz multiplicada = orig.prod(matriz3); 
        System.out.println("Matriz original: " + matriz3);
        System.out.println("Matriz multiplicada: " + multiplicada);
        
    }
}